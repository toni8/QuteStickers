package com.antonio.qutestickers.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.antonio.qutestickers.Helpers.Constants;
import com.antonio.qutestickers.Helpers.ProgressValueEventListener;
import com.antonio.qutestickers.Helpers.Utils;
import com.antonio.qutestickers.Models.User;
import com.antonio.qutestickers.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.antonio.qutestickers.Helpers.Utils.hideKeyboard;
import static com.antonio.qutestickers.Helpers.Utils.validateEmail;
import static com.antonio.qutestickers.Helpers.Utils.validatePassword;

public class LoginActivity extends AppCompatActivity {

    Button btnLoginOrSignup;
    Button btnChangeLayout;
    Boolean signingUp = true;
    RelativeLayout rlLoginPage;
    ProgressBar progressBar;

    TextInputLayout emailWrapper, passwordWrapper, confirmPasswordWrapper;
    TextView tvForgotPassword;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initFirebaseAuth();

        rlLoginPage = (RelativeLayout) findViewById(R.id.rl_login_layout);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        btnLoginOrSignup = (Button) findViewById(R.id.btn_login_or_signup);
        btnChangeLayout = (Button) findViewById(R.id.btn_change_login_layout);
        tvForgotPassword = (TextView) findViewById(R.id.tv_forgot_password);
        emailWrapper = (TextInputLayout) findViewById(R.id.emailWrapper);
        passwordWrapper = (TextInputLayout) findViewById(R.id.passwordWrapper);
        confirmPasswordWrapper = (TextInputLayout) findViewById(R.id.confirmPasswordWrapper);
        emailWrapper.setHint("Email");
        emailWrapper.setErrorTextAppearance(R.style.TextAppearance_App_ErrorText);
        passwordWrapper.setHint("Password");
        passwordWrapper.setErrorTextAppearance(R.style.TextAppearance_App_ErrorText);
        confirmPasswordWrapper.setHint("Confirm Password");
        confirmPasswordWrapper.setErrorTextAppearance(R.style.TextAppearance_App_ErrorText);

        btnLoginOrSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(LoginActivity.this);

                String email = emailWrapper.getEditText() != null ?
                        emailWrapper.getEditText().getText().toString() : "";
                String password = passwordWrapper.getEditText() != null ?
                        passwordWrapper.getEditText().getText().toString() : "";
                String confirmPassword = confirmPasswordWrapper.getEditText() != null ?
                        confirmPasswordWrapper.getEditText().getText().toString() : "";

                if (!validateEmail(email)) {
                    emailWrapper.setError("Invalid email address.");
                } else if (!validatePassword(password)) {
                    passwordWrapper.setError("Password must be at least 6 characters.");
                } else if (!confirmPassword.equals(password) && signingUp) {
                    confirmPasswordWrapper.setError("Passwords do not match.");
                } else {
                    emailWrapper.setErrorEnabled(false);
                    passwordWrapper.setErrorEnabled(false);

                    rlLoginPage.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);

                    if (signingUp) {
                        createAccount(email, password);
                    } else
                        signIn(email, password);
                }
            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(LoginActivity.this)
                        .title("Enter your email")
                        .backgroundColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary))
                        .itemsColor(ContextCompat.getColor(LoginActivity.this, R.color.colorAccent))
                        .negativeColor(ContextCompat.getColor(LoginActivity.this, R.color.colorAccent))
                        .contentColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimaryDarker))
                        .titleColor(ContextCompat.getColor(getApplicationContext(), R.color.black))
                        .inputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
                        .input("Email", "", new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                mAuth.sendPasswordResetEmail(input.toString())
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                                            .setTitleText("Email sent.")
                                                            .setContentText("A password reset email has been sent to your address.")
                                                            .show();
                                                } else {
                                                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                            .setTitleText("Email not sent.")
                                                            .setContentText("There has been an error. Please check your address and try again.")
                                                            .show();
                                                }
                                            }
                                        });
                            }
                        }).show();


            }
        });

        btnChangeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleLayout();
            }
        });

    }

    private void initFirebaseAuth() {
        mAuth = FirebaseAuth.getInstance();
    }

    private void createAccount(final String email, final String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(Constants.TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        if (!task.isSuccessful()) {
                            rlLoginPage.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            Utils.createErrorDialog(LoginActivity.this, "Error", task.getException() != null ?
                                    task.getException().getMessage() : "Registration failed");
                        } else {
                            enterUsername(email, password);
                        }

                    }
                });
    }

    Boolean nameExists = false;

    private void enterUsername(final String email, final String password) {
        new MaterialDialog.Builder(LoginActivity.this)
                .title("Enter your name")
                .cancelable(false)
                .content(nameExists ? "Username already exists" : "")
                .backgroundColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary))
                .itemsColor(ContextCompat.getColor(LoginActivity.this, R.color.colorAccent))
                .negativeColor(ContextCompat.getColor(LoginActivity.this, R.color.colorAccent))
                .contentColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimaryDarker))
                .titleColor(nameExists ? ContextCompat.getColor(getApplicationContext(), R.color.red)
                        : ContextCompat.getColor(getApplicationContext(), R.color.black))
                .canceledOnTouchOutside(false)
                .inputRangeRes(3, 12, R.color.red)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input("Username", "", new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                        verifyUsername(input.toString(), email, password);
                    }
                }).show();
    }


    private void verifyUsername(final String name, final String email, final String password) {

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("usernames");
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null)
            databaseReference.child(user.getUid());

        databaseReference.addListenerForSingleValueEvent(new ProgressValueEventListener(this) {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                super.onDataChange(dataSnapshot);
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    if (name.equalsIgnoreCase(ds.getKey())) {
                        nameExists = true;
                        enterUsername(email, password);
                        return;
                    }
                }
                addUsername(name, email, password);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                super.onCancelled(databaseError);
            }
        });
    }


    private void addUsername(final String input, final String email, final String password) {

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(input)
                .build();

        if (user != null)
            user.updateProfile(profileUpdates)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {

                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                DatabaseReference myRef =
                                        database.getReference("users/"
                                                + user.getUid());

                                User myUser = new User();
                                myUser.setDisplayName(input);
                                myUser.setUserId(user.getUid());
                                myRef.setValue(myUser);

                                myRef = database.getReference("usernames").child(user.getUid());
                                myRef.child("name").setValue(input);

                                Log.d(Constants.TAG, "User profile updated.");

                                ValueEventListener postListener = new ProgressValueEventListener(LoginActivity.this) {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        super.onDataChange(dataSnapshot);
                                        mAuth.signOut();
                                        signIn(email, password);
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        super.onCancelled(databaseError);
                                    }
                                };
                                myRef.addValueEventListener(postListener);


                            }
                        }
                    });

    }

    private void signIn(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof FirebaseAuthException) {
                    if (((FirebaseAuthException) e).getErrorCode().equalsIgnoreCase("ERROR_USER_NOT_FOUND")) {
                        Utils.createErrorDialog(LoginActivity.this, "Error",
                                "User not found with the specified credentials.");
                    } else {
                        Utils.createErrorDialog(LoginActivity.this, "Error",
                                e.getMessage());
                    }
                } else {
                    Utils.createErrorDialog(LoginActivity.this, "Error",
                            e.getMessage());
                }
            }
        })
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(Constants.TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            rlLoginPage.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);


                            Log.w(Constants.TAG, "signInWithEmail:failed", task.getException());

                        } else {
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            if (user != null) {
                                startActivity(new Intent(LoginActivity.this, SplashScreenActivity.class));
                                finish();
                            }
                        }
                    }
                });
    }


    private void toggleLayout() {
        signingUp = !signingUp;

        if (signingUp) {
            btnLoginOrSignup.setText("Sign Up");
            btnChangeLayout.setText("Already member? Log in!");
            if (confirmPasswordWrapper != null)
                confirmPasswordWrapper.setVisibility(View.VISIBLE);
        } else {
            btnLoginOrSignup.setText("Log In");
            btnChangeLayout.setText("New member? Sign up!");
            if (confirmPasswordWrapper != null)
                confirmPasswordWrapper.setVisibility(View.GONE);
        }
    }

}
