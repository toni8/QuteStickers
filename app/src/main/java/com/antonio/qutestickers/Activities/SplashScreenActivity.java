package com.antonio.qutestickers.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;
import com.antonio.qutestickers.Database.DaoMaster;
import com.antonio.qutestickers.Helpers.Constants;
import com.antonio.qutestickers.Helpers.MySingleton;
import com.antonio.qutestickers.Helpers.ProgressValueEventListener;
import com.antonio.qutestickers.Models.User;
import com.antonio.qutestickers.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.greenrobot.greendao.database.Database;

public class SplashScreenActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private static final int FLAG_GOTO_LOGIN = 0, FLAG_GOTO_MAIN_ACTIVITY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    if (user.getDisplayName() != null) {

                        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(SplashScreenActivity.this, Constants.DB_NAME + user.getDisplayName());
                        Database db = helper.getWritableDb();
                        DaoMaster dm = new DaoMaster(db);
                        MySingleton.getInstance().setUserDbDao(dm.newSession().getUserDbDao());
                        MySingleton.getInstance().setLocalDatabase(db);

                        ///// Using the below lines of code we can toggle ENCRYPTED to true or false in other to use either an encrypted database or not.
//      DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, ENCRYPTED ? "users-db-encrypted" : "users-db");
//      Database db = ENCRYPTED ? helper.getEncryptedWritableDb("super-secret") : helper.getWritableDb();
//      daoSession = new DaoMaster(db).newSession();

                        MySingleton.getInstance().setCurrentUser(user);
                        updateToken(user);

                    } else {
                        enterUsername();
                    }
                } else {
                    // User is signed out
                    Log.d(Constants.TAG, "onAuthStateChanged:signed_out");

                    checkAppVersion(FLAG_GOTO_LOGIN);


                }
                // ...
            }
        };

    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private Boolean nameExists = false;

    private void enterUsername() {
        new MaterialDialog.Builder(SplashScreenActivity.this)
                .title("Enter your name")
                .cancelable(false)
                .content(nameExists ? "Username already exists" : "")
                .titleColor(nameExists ? ContextCompat.getColor(getApplicationContext(), R.color.red)
                        : ContextCompat.getColor(getApplicationContext(), R.color.black))
                .canceledOnTouchOutside(false)
                .inputRangeRes(3, 12, R.color.red)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input("Username", "", new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                        verifyUsername(input.toString());
                    }
                }).show();
    }


    private void verifyUsername(final String name) {

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("usernames");
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null)
            databaseReference.child(user.getUid());

        databaseReference.addListenerForSingleValueEvent(new ProgressValueEventListener(this) {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                super.onDataChange(dataSnapshot);
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    if (name.equalsIgnoreCase(ds.getKey())) {
                        nameExists = true;
                        enterUsername();
                        return;
                    }
                }
                addUsername(name);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                super.onCancelled(databaseError);
            }
        });
    }


    private void addUsername(final String input) {

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(input)
                .build();

        if (user != null)
            user.updateProfile(profileUpdates)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {

                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                DatabaseReference myRef =
                                        database.getReference("users/"
                                                + user.getUid());

                                User myUser = new User();
                                myUser.setDisplayName(input);
                                myUser.setUserId(user.getUid());
                                myRef.setValue(myUser);

                                myRef = database.getReference("usernames").child(user.getUid());
                                myRef.child("name").setValue(input);

                                Log.d(Constants.TAG, "User profile updated.");

                                ValueEventListener postListener = new ProgressValueEventListener(SplashScreenActivity.this) {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        super.onDataChange(dataSnapshot);
                                        mAuth.signOut();
                                        finish();
                                        startActivity(getIntent());
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        super.onCancelled(databaseError);
                                    }
                                };
                                myRef.addValueEventListener(postListener);


                            }
                        }
                    });

    }

    private void updateToken(FirebaseUser user) {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        final DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("users/"
                + user.getUid()
                + "/tokens/");
        if (refreshedToken != null)
            myRef.child(refreshedToken).setValue(true);

        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }

        checkAppVersion(FLAG_GOTO_MAIN_ACTIVITY);
    }


    private void checkAppVersion(final int FLAG) {

        if (FLAG == FLAG_GOTO_MAIN_ACTIVITY) {
            startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
            finish();
        } else {
            startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
            finish();
        }

        //leave out version checking for now
       /*
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("appInfo");
        databaseReference.keepSynced(false);
        databaseReference.addListenerForSingleValueEvent(new ProgressValueEventListener(this) {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                super.onDataChange(dataSnapshot);
                Boolean forceUpdate = false;
                long versionCode = 0;

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (snapshot.getKey().equalsIgnoreCase("forceUpdate"))
                        forceUpdate = (Boolean) snapshot.getValue();
                    if (snapshot.getKey().equalsIgnoreCase("versionCode"))
                        versionCode = (long) snapshot.getValue();
                }

                if (versionCode > Utils.getAppVersionCode(SplashScreenActivity.this)) {
                    //new version available
                    if (forceUpdate) {
                        //must update
                        Toast.makeText(SplashScreenActivity.this, "MUST UPDATE", Toast.LENGTH_SHORT).show();
                        Utils.openPlayStore(SplashScreenActivity.this);
                    } else {
                        //just notify user
                        Toast.makeText(SplashScreenActivity.this, "NEW VERSION AVAILABLE", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //just notify user
                    Toast.makeText(SplashScreenActivity.this, "NO NEW VERSION - PROCEED", Toast.LENGTH_SHORT).show();
                    if (FLAG == FLAG_GOTO_MAIN_ACTIVITY) {
                        startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                        finish();
                    } else {
                        startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
                        finish();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                super.onCancelled(databaseError);
            }
        });
        */
    }

}
