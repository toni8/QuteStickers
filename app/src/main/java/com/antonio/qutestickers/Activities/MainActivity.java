package com.antonio.qutestickers.Activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.TextView;

import com.antonio.qutestickers.Fragments.LocalStickersFragment;
import com.antonio.qutestickers.Fragments.OnlineStickersFragment;
import com.antonio.qutestickers.Fragments.ReceivedStickersFragment;
import com.antonio.qutestickers.Fragments.SettingsFragment;
import com.antonio.qutestickers.Helpers.MySingleton;
import com.antonio.qutestickers.Helpers.ProgressValueEventListener;
import com.antonio.qutestickers.Models.ReceivedSticker;
import com.antonio.qutestickers.R;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.antonio.qutestickers.Fragments.SettingsFragment.SETTINGS_FRAGMENT_TAG;

public class MainActivity extends BaseActivity implements OnlineStickersFragment.OnFragmentInteractionListener,
        LocalStickersFragment.OnFragmentInteractionListener, ReceivedStickersFragment.OnReceivedStickersChangedListener,
        SettingsFragment.OnFragmentInteractionListener {

    private TextView mTextMessage;
    AHBottomNavigation navigation;
    String FRAGMENT_TAG = LocalStickersFragment.LOCAL_FRAGMENT_TAG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.app_name));

        navigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        AHBottomNavigationAdapter navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.navigation);
        navigationAdapter.setupWithBottomNavigation(navigation);

        navigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                changeFragment(position);
                return true;
            }
        });

        getSupportFragmentManager().beginTransaction().add(
                R.id.fragmentContainer, new LocalStickersFragment())
                .commit();

        downloadReceivedStickers();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_choose_sticker;
    }


    private void changeFragment(int position) {

        Fragment newFragment = null;

        //I want to clear backstack here/temp solution
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }

        if (position == 0) {
            FRAGMENT_TAG = LocalStickersFragment.LOCAL_FRAGMENT_TAG;
            newFragment = new LocalStickersFragment();
        } else if (position == 1) {
            FRAGMENT_TAG = OnlineStickersFragment.ONLINE_FRAGMENT_TAG;
            newFragment = new OnlineStickersFragment();
        } else {
            FRAGMENT_TAG = ReceivedStickersFragment.RECEIVED_FRAGMENT_TAG;
            newFragment = new ReceivedStickersFragment();
        }
        getSupportFragmentManager().beginTransaction().replace(
                R.id.fragmentContainer, newFragment, FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().findFragmentByTag(SETTINGS_FRAGMENT_TAG) != null
                && !getSupportFragmentManager().findFragmentByTag(SETTINGS_FRAGMENT_TAG).isResumed()) {
            getSupportFragmentManager().popBackStack(SettingsFragment.class.getName(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            super.onBackPressed();
        }

    }


    private void updateNotificationBadge(int unread) {
        if (unread != 0)
            navigation.setNotification(String.valueOf(unread), 2);
        else
            navigation.setNotification("", 2);
    }

    private void downloadReceivedStickers() {
        final List<ReceivedSticker> receivedStickers = new ArrayList<>();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users").child(MySingleton.getInstance()
                .getCurrentUser().getUid())
                .child("stickers");

        ref.keepSynced(true);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int unseenStickers = 0;
                receivedStickers.clear();
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    ReceivedSticker receivedSticker = snap.getValue(ReceivedSticker.class);
                    if (receivedSticker != null) {
                        receivedSticker.setStickerID(snap.getKey());
                        receivedStickers.add(receivedSticker);
                        if (!receivedSticker.isSeen())
                            unseenStickers++;
                    }
                }
                updateNotificationBadge(unseenStickers);
                Collections.reverse(receivedStickers);
                MySingleton.getInstance().setReceivedStickers(receivedStickers);

                ReceivedStickersFragment receivedStickersFragment = (ReceivedStickersFragment) getSupportFragmentManager()
                        .findFragmentByTag(ReceivedStickersFragment.RECEIVED_FRAGMENT_TAG);

                if (receivedStickersFragment != null) {
                    receivedStickersFragment.getStickers();
                    //receivedStickersFragment.hideProgressBar();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    @Override
    public void onReceivedStickersChanged(int unread) {

    }
}
