package com.antonio.qutestickers.Activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.antonio.qutestickers.Adapters.FriendsAdapter;
import com.antonio.qutestickers.Database.UserDb;
import com.antonio.qutestickers.Database.UserDbDao;
import com.antonio.qutestickers.Helpers.ConnectionDetector;
import com.antonio.qutestickers.Helpers.Constants;
import com.antonio.qutestickers.Helpers.DbUtils;
import com.antonio.qutestickers.Helpers.MySingleton;
import com.antonio.qutestickers.Helpers.ProgressValueEventListener;
import com.antonio.qutestickers.Helpers.Utils;
import com.antonio.qutestickers.Models.ReceivedSticker;
import com.antonio.qutestickers.Models.Sticker;
import com.antonio.qutestickers.Models.User;
import com.antonio.qutestickers.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ChooseFriendActivity extends BaseActivity implements FriendsAdapter.FriendClickedCaller {

    RecyclerView recyclerView;
    List<UserDb> recentUsers;
    List<String> suggestedUsers;
    List<User> usersWithKeys;
    FriendsAdapter friendsAdapter;
    private AutoCompleteTextView atv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setToolbarTitle("Choose friend");
        showBackButton();

        recentUsers = new ArrayList<>();
        suggestedUsers = new ArrayList<>();
        usersWithKeys = new ArrayList<>();

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference mRef = firebaseDatabase.getReference().child("usernames");
        mRef.keepSynced(false);
        Query query = mRef.orderByChild("name");
        query.addListenerForSingleValueEvent(new ProgressValueEventListener(ChooseFriendActivity.this) {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                super.onDataChange(snapshot);

                suggestedUsers.clear();
                for (DataSnapshot ds : snapshot.getChildren()) {
                    if (!MySingleton.getInstance().getCurrentUser().getDisplayName()
                            .equalsIgnoreCase(ds.child("name").getValue().toString())) {
                        User user = new User();
                        user.setDisplayName(ds.child("name").getValue().toString());
                        user.setUserId(ds.getKey());
                        usersWithKeys.add(user);
                        suggestedUsers.add(ds.child("name").getValue().toString());
                    }
                }
                String[] users = new String[suggestedUsers.size()];
                users = suggestedUsers.toArray(users);
                setupAutocompleteTextView(users);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                super.onCancelled(error);
            }
        });

        recentUsers.addAll(DbUtils.getFromSQL());
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewFriends);
        friendsAdapter = new FriendsAdapter(this, recentUsers);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(friendsAdapter);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_choose_friend;
    }


    private void setupAutocompleteTextView(String[] users) {
        atv = (AutoCompleteTextView) findViewById(R.id.atv_friend_search);
        atv.setThreshold(1);
        final ArrayAdapter adapter = new ArrayAdapter<>(ChooseFriendActivity.this,
                android.R.layout.simple_dropdown_item_1line, users);
        atv.setAdapter(adapter);
        atv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                userChosen();
            }
        });
    }

    public void userChosen() {
        if (ConnectionDetector.isDataConnectionAvailable(getApplicationContext())) {
            new MaterialDialog.Builder(this)
                    .title("Enter message")
                    .cancelable(false)
                    .canceledOnTouchOutside(false)
                    .negativeText("Cancel")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .inputRangeRes(0, 24, R.color.red)
                    .inputType(InputType.TYPE_CLASS_TEXT)
                    .input("Message", "", new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                            for (User user : usersWithKeys) {
                                if (user.getDisplayName().equalsIgnoreCase(atv.getText().toString())) {
                                    UserDb userDb = new UserDb();
                                    userDb.setDisplayName(atv.getText().toString());
                                    userDb.setUid(user.getUserId());
                                    userDb.setId(null);
                                    userDb.setTimesSent(1);
                                    DbUtils.SaveToSQL(userDb);

                                    recentUsers.clear();
                                    recentUsers.addAll(DbUtils.getFromSQL());
                                    friendsAdapter.notifyDataSetChanged();

                                    sendSticker(input.toString());
                                    return;
                                }
                            }
                        }
                    }).show();
        } else {
            Utils.createErrorDialog(ChooseFriendActivity.this, "Network error",
                    "Please check your internet connection and try again.");
        }

    }

    private void sendSticker(final String message) {


        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference mRef = firebaseDatabase.getReference().child("usernames");
        Query query = mRef.orderByChild("name").equalTo(atv.getText().toString());
        query.addListenerForSingleValueEvent(new ProgressValueEventListener(ChooseFriendActivity.this) {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                super.onDataChange(dataSnapshot);
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    final FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference myRef = database.getReference("stickers/").push();

                    String stickerUrl = "";
                    Intent intent = getIntent();
                    if (intent.getExtras() != null) {
                        stickerUrl = intent.getExtras().getString(Constants.EXTRA_STICKER_URL);
                    }

                    Sticker sticker = new Sticker();
                    sticker.setReceiver(ds.getKey());
                    sticker.setAuthor(FirebaseAuth.getInstance().getCurrentUser().getUid());
                    sticker.setMessage(message);
                    sticker.setStickerURL(stickerUrl);
                    myRef.setValue(sticker);

                    String stickerID = myRef.getKey();
                    ReceivedSticker receivedSticker = new ReceivedSticker();
                    receivedSticker.setMessage(message);
                    receivedSticker.setAuthor(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
                    receivedSticker.setSeen(false);

                    myRef = database.getReference("users/" + ds.getKey() + "/stickers").child(stickerID);
                    myRef.setValue(receivedSticker);

                    createAchievement();

                    break;
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                super.onCancelled(databaseError);
            }
        });
    }

    private void createAchievement() {
        Boolean showAchievementPopup = false;
        String textMsg = "";
        int img = R.drawable.jelly_sticker_classic;
        for (UserDb udb : MySingleton.getInstance().getUserDbDao()
                .queryBuilder().orderDesc(UserDbDao.Properties.Id).build().list()) {
            if (udb.getDisplayName().equalsIgnoreCase(atv.getText().toString())) {
                if (udb.getTimesSent() == Constants.ACHIEVEMENT_CLASSIC) {
                    showAchievementPopup = true;
                    textMsg = getString(R.string.first_achievement, atv.getText().toString());
                } else if (udb.getTimesSent() == Constants.ACHIEVEMENT_YAYZ) {
                    showAchievementPopup = true;
                    textMsg = "YAYZ!";
                    img = R.drawable.jelly_sticker_yayz;
                } else if (udb.getTimesSent() == Constants.ACHIEVEMENT_AWESOME) {
                    showAchievementPopup = true;
                    textMsg = "AWESOME!";
                    img = R.drawable.jelly_sticker_awesome;
                } else if (udb.getTimesSent() == Constants.ACHIEVEMENT_ZOMFJ) {
                    showAchievementPopup = true;
                    textMsg = "ZOMFJ!";
                    img = R.drawable.jelly_sticker_zomfj;
                }
                break;
            }
        }

        if (showAchievementPopup)
            createAchievementDialog(textMsg, img).show();
        else {
            SweetAlertDialog dialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Sticker successfully sent!");

            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            });

            dialog.show();
        }


    }

    private Dialog createAchievementDialog(String msg, int img) {
        final Dialog dialog = new Dialog(this);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_achievement);
        ImageView ivAchievement = (ImageView) dialog.findViewById(R.id.iv_achievement);
        ivAchievement.setImageResource(img);
        TextView tvAchievement = (TextView) dialog.findViewById(R.id.tv_achievement);
        tvAchievement.setText(msg);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });
        return dialog;
    }

    @Override
    public void userChosenFromExistingList(String name) {
        atv.setText(name);
        userChosen();
    }
}
