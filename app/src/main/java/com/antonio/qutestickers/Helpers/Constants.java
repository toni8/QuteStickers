package com.antonio.qutestickers.Helpers;

/**
 * Created by Antonio on 14-May-17.
 */

public class Constants {
    public static final String TAG = "QuteStickersLOG";
    public static final String DB_NAME = "0ObHgOD2dG0JQz";
    public static final String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";
    public static final String EXTRA_STICKER_URL = "sticker_url_extra";

    //ACHIEVEMENTS CHECKPOINTS
    public static final int ACHIEVEMENT_CLASSIC = 1;
    public static final int ACHIEVEMENT_YAYZ = 7;
    public static final int ACHIEVEMENT_AWESOME = 15;
    public static final int ACHIEVEMENT_ZOMFJ = 25;

    public static final int NUMBER_OF_COLUMNS = 2;
}
