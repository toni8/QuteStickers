package com.antonio.qutestickers.Helpers;

import android.app.Activity;
import android.app.Dialog;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Antonio on 18-May-17.
 */

public class ProgressValueEventListener implements ValueEventListener {
    private Dialog progressDialog;

    public ProgressValueEventListener(Activity act) {
        progressDialog = Utils.createLoadingDialog(act);
        progressDialog.show();
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        Log.i(Constants.TAG, "onDataChange: " + dataSnapshot.getValue());
        progressDialog.dismiss();
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

        Log.w(Constants.TAG, "onCancelled: " + databaseError.getMessage());
        progressDialog.dismiss();
    }
}
