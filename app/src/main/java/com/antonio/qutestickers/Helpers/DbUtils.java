package com.antonio.qutestickers.Helpers;

import com.antonio.qutestickers.Database.DaoMaster;
import com.antonio.qutestickers.Database.UserDb;
import com.antonio.qutestickers.Database.UserDbDao;

import java.util.List;

/**
 * Created by Programmer on 16-May-17.
 */

public class DbUtils {

    public static void SaveToSQL(UserDb userDb) {
        for (UserDb udb : MySingleton.getInstance().getUserDbDao()
                .queryBuilder().orderDesc(UserDbDao.Properties.Id).build().list()) {
            if (udb.getDisplayName().equalsIgnoreCase(userDb.getDisplayName())) {
                userDb.setId(udb.getId());
                userDb.setTimesSent((udb.getTimesSent() == null || udb.getTimesSent() == 0 ? 1 :
                        udb.getTimesSent()) + 1);
                MySingleton.getInstance().getUserDbDao().update(userDb);

                return;
            }
        }
        MySingleton.getInstance().getUserDbDao().insert(userDb);
    }

    public static List<UserDb> getFromSQL() {
        return MySingleton.getInstance().getUserDbDao().queryBuilder().orderDesc(UserDbDao.Properties.Id).build().list();
    }

    public void clearSQLdatabase() {
        MySingleton.getInstance().getDaoMaster();
        DaoMaster.dropAllTables(MySingleton.getInstance().getLocalDatabase(), true);
        MySingleton.getInstance().getDaoMaster();
        DaoMaster.createAllTables(MySingleton.getInstance().getLocalDatabase(), true);
    }

}
