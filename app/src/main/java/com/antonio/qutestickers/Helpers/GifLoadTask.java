package com.antonio.qutestickers.Helpers;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.antonio.qutestickers.ViewStickerActivity;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * Created by Programmer on 11-May-17.
 */

@RequiresApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class GifLoadTask extends FutureTask<ByteBuffer> {

    private static final String DEF_URL = "https://media.giphy.com/media/dFa6ogMFIe9nq/giphy.gif";
    private final WeakReference<ViewStickerActivity> mFragmentReference;

    public GifLoadTask(ViewStickerActivity httpFragment, final String gifURL) {
        super(new Callable<ByteBuffer>() {
            @Override
            public ByteBuffer call() throws Exception {
                URLConnection urlConnection = new URL(gifURL == null ? DEF_URL : gifURL).openConnection();
                Log.d("STICKER", "URL: " + (gifURL == null ? DEF_URL : gifURL));
                urlConnection.connect();
                final int contentLength = urlConnection.getContentLength();
                if (contentLength < 0) {
                    throw new IOException("Content-Length not present");
                }
                ByteBuffer buffer = ByteBuffer.allocateDirect(contentLength);
                ReadableByteChannel channel = Channels.newChannel(urlConnection.getInputStream());
                while (buffer.remaining() > 0)
                    channel.read(buffer);
                channel.close();
                return buffer;
            }
        });
        mFragmentReference = new WeakReference<>(httpFragment);
    }

    @Override
    protected void done() {
        final ViewStickerActivity httpFragment = mFragmentReference.get();
        if (httpFragment == null) {
            return;
        }
        try {
            httpFragment.onGifDownloaded(get());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            httpFragment.onDownloadFailed(e);
        }
    }
}