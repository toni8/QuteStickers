package com.antonio.qutestickers.Helpers;

import com.antonio.qutestickers.AppController;
import com.antonio.qutestickers.Database.DaoMaster;
import com.antonio.qutestickers.Database.UserDbDao;
import com.antonio.qutestickers.Models.ReceivedSticker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.greenrobot.greendao.database.Database;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Programmer on 16-May-17.
 */

public class MySingleton {

    private static MySingleton mInstance = null;
    private UserDbDao userDbDao;
    private Database localDatabase;
    private DaoMaster daoMaster;
    private FirebaseUser currentUser;
    private List<ReceivedSticker> receivedStickers;

    public static MySingleton getInstance() {
        if (mInstance == null) {
            mInstance = new MySingleton();
        }
        return mInstance;
    }

    public List<ReceivedSticker> getReceivedStickers() {
        if(receivedStickers == null)
            receivedStickers = new ArrayList<>();
        return receivedStickers;
    }

    public void setReceivedStickers(List<ReceivedSticker> receivedStickers) {
        this.receivedStickers = receivedStickers;
    }

    public FirebaseUser getCurrentUser() {
        if (currentUser == null)
            currentUser = FirebaseAuth.getInstance().getCurrentUser();
        return currentUser;
    }

    public void setCurrentUser(FirebaseUser currentUser) {
        this.currentUser = currentUser;
    }

    public Database getLocalDatabase() {
        return localDatabase;
    }

    public void setLocalDatabase(Database localDatabase) {
        this.localDatabase = localDatabase;
    }

    public DaoMaster getDaoMaster() {
        return daoMaster;
    }

    public void setDaoMaster(DaoMaster daoMaster) {
        this.daoMaster = daoMaster;
    }

    public UserDbDao getUserDbDao() {
        if (userDbDao == null) {
            AppController appController = new AppController();
            userDbDao = appController.getDaoSession().getUserDbDao();
        }
        return userDbDao;
    }

    public void setUserDbDao(UserDbDao userDbDao) {
        this.userDbDao = userDbDao;
    }
}
