package com.antonio.qutestickers.Helpers;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import com.antonio.qutestickers.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.antonio.qutestickers.Helpers.Constants.EMAIL_PATTERN;

/**
 * Created by Antonio on 14-May-17.
 */

public class Utils {
    public static void hideKeyboard(Activity act) {
        View view = act.getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static boolean validateEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static Dialog createLoadingDialog(Activity act) {
        final Dialog dialog = new Dialog(act);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.custom_progress_dialog);
        return dialog;
    }

    public static void createErrorDialog(Activity act, String title, String msg) {
        new SweetAlertDialog(act, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(msg)
                .setConfirmText("Close")
                .show();

     /*   return new MaterialDialog.Builder(act)
                .title(title)
                .content(msg)
                .titleColor(ContextCompat.getColor(act, R.color.colorAccentDark))
                .backgroundColor(ContextCompat.getColor(act, R.color.colorPrimary))
                .itemsColor(ContextCompat.getColor(act, R.color.colorAccent))
                .negativeColor(ContextCompat.getColor(act, R.color.colorAccent))
                .contentColor(ContextCompat.getColor(act, R.color.colorPrimaryDarker))
                .negativeText("Close").build();*/
    }

    public static boolean validatePassword(String password) {
        return password.length() > 5;
    }

    /**
     * Specify the name of a drawable and it will return it's generated ID
     *
     * @param act          The current activity
     * @param drawableName Drawable's name
     * @return the id of that drawable
     */
    public static int getDrawableId(Activity act, String drawableName) {
        return act.getResources().getIdentifier(drawableName, "raw", act.getPackageName());
    }

    /**
     * Gets the current version name
     * @param act Activity
     * @return version's name
     */
    public static String getAppVersionName(Activity act) {
        PackageInfo pInfo = getPackageInfo(act);
        String version = "1.0";
        if (pInfo != null)
            version = pInfo.versionName;

        return version;
    }

    /**
     * Gets the current version code
     * @param act Activity
     * @return version's code
     */
    public static int getAppVersionCode(Activity act) {
        PackageInfo pInfo = getPackageInfo(act);
        int verCode = 0;
        if (pInfo != null)
            verCode = pInfo.versionCode;

        return verCode;
    }

    private static PackageInfo getPackageInfo(Activity act) {
        PackageInfo pInfo = null;
        try {
            pInfo = act.getPackageManager().getPackageInfo(act.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pInfo;
    }

    /**
     * Opens app on play store
     * @param act Activity
     */
    public static void openPlayStore(Activity act){
        final String appPackageName = act.getPackageName(); // getPackageName() from Context or Activity object
        try {
            act.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            act.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
