package com.antonio.qutestickers.Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 14-May-17.
 */

public class User {

    List<Sticker> stickers;
    String userId;
    String displayName;

    public List<Sticker> getStickers() {
        if(stickers == null)
            stickers = new ArrayList<>();
        return stickers;
    }

    public void setStickers(List<Sticker> stickers) {
        this.stickers = stickers;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
