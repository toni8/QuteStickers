package com.antonio.qutestickers.Models;

/**
 * Created by Programmer on 23-May-17.
 */

public class ReceivedSticker {
    private String author, message, stickerID;
    private Boolean seen;

    public String getStickerID() {
        return stickerID;
    }

    public void setStickerID(String stickerID) {
        this.stickerID = stickerID;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean isSeen() {
        if(seen == null)
            seen = false;
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }
}
