package com.antonio.qutestickers;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.antonio.qutestickers.Helpers.MySingleton;
import com.antonio.qutestickers.Helpers.Utils;
import com.google.firebase.database.FirebaseDatabase;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import net.bohush.geometricprogressview.GeometricProgressView;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;
import pl.droidsonroids.gif.GifTextureView;

public class ViewStickerActivity extends AppCompatActivity implements View.OnClickListener {

    GifImageView giv;
    GeometricProgressView geometricProgressView;
    ImageButton btnClose;
    String stickerUrl;

    /////
    private PopupWindow pw;
    String gifUrl = "http://i.kinja-img.com/gawker-media/image/upload/s--B7tUiM5l--/gf2r69yorbdesguga10i.gif";
    String cuteGifUrl = "http://bestanimations.com/Animals/Mammals/Cats/cats/cute-kitty-animated-gif-25.gif";
    private GifTextureView mGifTextureView;
    private final ExecutorService mExecutorService = Executors.newSingleThreadExecutor();
    ////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        setTitle("");

        Intent intent_o = getIntent();
        if (intent_o != null) {
            String author = intent_o.getStringExtra("author");
            String message = intent_o.getStringExtra("message");
            String stickerID = intent_o.getStringExtra("stickerID");
            markStickerSeen(stickerID);
            stickerUrl = intent_o.getStringExtra("stickerURL");
            TextView tvAuthor = (TextView) findViewById(R.id.tv_author);
            tvAuthor.setText(author == null || author.isEmpty() ? "Unknown" : author);

            TextView tvMsg = (TextView) findViewById(R.id.tv_message);
            tvMsg.setText(message);
            Log.d("STICKER", "URL: " + stickerUrl);
        }

        giv = (GifImageView) findViewById(R.id.iv_sticker);

        geometricProgressView = (GeometricProgressView) findViewById(R.id.progressView);
        btnClose = (ImageButton) findViewById(R.id.btn_close);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (stickerUrl.contains("gif") || stickerUrl.contains("png")) {
            downloadGif();
        } else {
            giv.setImageResource(Utils.getDrawableId(this, stickerUrl) == 0 ?
                    Utils.getDrawableId(this, "sticker_not_found") :
                    Utils.getDrawableId(this, stickerUrl));

            geometricProgressView.setVisibility(View.INVISIBLE);
        }
    }

    private void markStickerSeen(String stickerID){
        FirebaseDatabase.getInstance().getReference("users").child(MySingleton.getInstance()
                .getCurrentUser().getUid())
                .child("stickers")
                .child(stickerID)
                .child("seen").setValue(true);
    }

    @RequiresApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void onGifDownloaded(ByteBuffer buffer) {

        try {
            geometricProgressView.setVisibility(View.INVISIBLE);
            GifDrawable gifFromBytes = new GifDrawable(buffer);
            giv.setImageDrawable(gifFromBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //    mGifTextureView.setInputSource(new InputSource.DirectByteBufferSource(buffer));
    }

    @RequiresApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void onDownloadFailed(Exception e) {
        giv.setOnClickListener(ViewStickerActivity.this);

        Toast.makeText(this, "Fail: " + e.getMessage(), Toast.LENGTH_SHORT).show();

    }

    @RequiresApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private void downloadGif() {
        //temp solution - find something else this lags
        Ion.with(giv).load(stickerUrl).setCallback(new FutureCallback<ImageView>() {
            @Override
            public void onCompleted(Exception e, ImageView result) {
                geometricProgressView.setVisibility(View.INVISIBLE);
            }
        });
        // mExecutorService.submit(new GifLoadTask(this, stickerUrl));
    }

    @Override
    public void onClick(View v) {
        downloadGif();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
