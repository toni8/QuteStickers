package com.antonio.qutestickers;

import android.app.Application;

import com.antonio.qutestickers.Database.DaoSession;

/**
 * Created by Programmer on 16-May-17.
 */

public class AppController extends Application {

    public static final boolean ENCRYPTED = false;
    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();

       // FirebaseDatabase.getInstance().setPersistenceEnabled(true);


    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

}
