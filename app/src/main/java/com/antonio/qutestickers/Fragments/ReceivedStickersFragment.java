package com.antonio.qutestickers.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.antonio.qutestickers.Adapters.ReceivedStickersAdapter;
import com.antonio.qutestickers.Helpers.MySingleton;
import com.antonio.qutestickers.Models.ReceivedSticker;
import com.antonio.qutestickers.R;

import java.util.ArrayList;
import java.util.List;

import static com.antonio.qutestickers.Fragments.SettingsFragment.SETTINGS_FRAGMENT_TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReceivedStickersFragment.OnReceivedStickersChangedListener} interface
 * to handle interaction events.
 */
public class ReceivedStickersFragment extends Fragment {

    public static String RECEIVED_FRAGMENT_TAG = "ReceivedFragmentTag";

    private OnReceivedStickersChangedListener mListener;
    private List<ReceivedSticker> receivedStickers;
    private ReceivedStickersAdapter receivedStickersAdapter;
    private TextView tvNoReceivedStickers;
    private ProgressBar progressBar;

    public ReceivedStickersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_received_stickers, container, false);

        receivedStickers = new ArrayList<>();
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        tvNoReceivedStickers = (TextView) view.findViewById(R.id.tv_no_received_stickers);
        TextView tvAccSettings = (TextView) view.findViewById(R.id.tv_account_name);
        tvAccSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                        .beginTransaction();

                SettingsFragment frag = new SettingsFragment();
                transaction.replace(R.id.fragmentContainer, frag, SETTINGS_FRAGMENT_TAG);
                transaction.addToBackStack(frag.getClass().getName());
                transaction.commit();
            }
        });

        tvAccSettings.setText(MySingleton.getInstance().getCurrentUser().getDisplayName());
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        receivedStickersAdapter = new ReceivedStickersAdapter(getActivity(),
                ReceivedStickersFragment.this,
                receivedStickers);
        recyclerView.setAdapter(receivedStickersAdapter);

   //     showProgressBar();
        getStickers();
        return view;
    }

    public void getStickers() {
        receivedStickers.clear();
        receivedStickers.addAll(MySingleton.getInstance().getReceivedStickers());
        receivedStickersAdapter.notifyDataSetChanged();

        if (receivedStickersAdapter.getItemCount() == 0)
            tvNoReceivedStickers.setVisibility(View.VISIBLE);
        else
            tvNoReceivedStickers.setVisibility(View.GONE);
    }

    public void showProgressBar() {
        if (progressBar != null)
            progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    public void onDataDownloaded(int unread) {
        if (mListener != null) {
            mListener.onReceivedStickersChanged(unread);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnReceivedStickersChangedListener) {
            mListener = (OnReceivedStickersChangedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnReceivedStickersChangedListener {
        void onReceivedStickersChanged(int unread);
    }

}
