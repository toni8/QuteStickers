package com.antonio.qutestickers.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.antonio.qutestickers.Activities.LoginActivity;
import com.antonio.qutestickers.Activities.MainActivity;
import com.antonio.qutestickers.Helpers.Constants;
import com.antonio.qutestickers.Helpers.Utils;
import com.antonio.qutestickers.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SettingsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class SettingsFragment extends Fragment {

    public static String SETTINGS_FRAGMENT_TAG = "SettingsFragmentTag";

    private OnFragmentInteractionListener mListener;

    TextView tvLogout, tvAbout;

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        ((MainActivity) getActivity()).showBackButton();

        if (((MainActivity) getActivity()).getSupportActionBar() != null) {
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        tvAbout = (TextView) view.findViewById(R.id.tv_about);
        tvAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE)
                        .setTitleText("About")
                        .setContentText("v" + Utils.getAppVersionName(getActivity()) + "\n" +
                                "Antonio Nikoloski © 2017" + "\n" +
                        "Animations from www.giphy.com")
                        .setConfirmText("CLOSE")
                        .show();
            }
        });

        tvLogout = (TextView) view.findViewById(R.id.tv_logout);
        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Warning")
                        .setContentText("Are you sure you want to log out?")
                        .setConfirmText("Log me out")
                        .setCancelText("Cancel")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                signOut();
                            }
                        })
                        .show();
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((MainActivity) getActivity()).hideBackButton();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void signOut() {
        final Dialog dialog = Utils.createLoadingDialog(getActivity());
        dialog.show();

        final FirebaseAuth mAuth = FirebaseAuth.getInstance();

        FirebaseAuth.AuthStateListener mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d(Constants.TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    dialog.dismiss();
                    Log.d(Constants.TAG, "onAuthStateChanged:signed_out");
                    mAuth.removeAuthStateListener(this);
                    startActivity(new Intent(getActivity(), LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    getActivity().finish();
                }
                // ...
            }
        };

        disableToken(FirebaseAuth.getInstance().getCurrentUser());
        mAuth.addAuthStateListener(mAuthListener);
        mAuth.signOut();
    }

    private void disableToken(FirebaseUser user) {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        final DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("users/"
                + user.getUid()
                + "/tokens/");
        if (refreshedToken != null)
            myRef.child(refreshedToken).setValue(false);

    }
}
