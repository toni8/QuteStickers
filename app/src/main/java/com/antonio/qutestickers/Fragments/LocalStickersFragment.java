package com.antonio.qutestickers.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.antonio.qutestickers.Adapters.StickerListAdapter;
import com.antonio.qutestickers.Helpers.Constants;
import com.antonio.qutestickers.Models.Sticker;
import com.antonio.qutestickers.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class LocalStickersFragment extends BaseFragment {

    public static String LOCAL_FRAGMENT_TAG = "LocalFragmentTag";

    private OnFragmentInteractionListener mListener;
    private RecyclerView mRecyclerView;
    private List<Sticker> localStickers;
    private String[] localStickersNames = {"sticker_1", "sticker_yup_yup", "sticker_raindeer_bell",
            "sticker_chicken_angry", "cat_chasing_heart",
            "minion_guitar", "sticker_chicken_annoy", "sticker_chicken_dancing", "sticker_crying_on_ground",
            "sticker_cute_heart", "sticker_dog_dance", "sticker_dog_hi", "sticker_eatin_chips",
            "sticker_fat_cat_angry", "sticker_friends_highfive", "sticker_girl_goodnight",
            "sticker_sheep_chillin", "sticker_three_friends_love", "sticker_tongue_out",
            "sticker_waiting_dinner",
            "spider", "sticker_fuckboy", "sticker_great_job", "sticker_2", "sticker_3", "sticker_4",
            "sticker_5", "sticker_6", "sticker_7", "sticker_girl_behind", "sticker_hand_cigar",
            "sticker_hands_holding_fingers", "sticker_hands_universe", "sticker_psycho_heart",
            "sticker_unicorn", "sticker_wished_for_you", "sticker_you_are_my_sun"};

    public LocalStickersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_local_stickers, container, false);

        localStickers = new ArrayList<>();
        populateStickers();

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        // mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), Constants.NUMBER_OF_COLUMNS));
        mRecyclerView.setAdapter(new StickerListAdapter(getActivity(), localStickers));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing_12dp);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private void populateStickers() {
        for (String localStickersName : localStickersNames) {
            Sticker sticker = new Sticker();
            sticker.setStickerURL(localStickersName);
            localStickers.add(sticker);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
