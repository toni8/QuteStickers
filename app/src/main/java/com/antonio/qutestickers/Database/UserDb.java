package com.antonio.qutestickers.Database;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "USER_DB".
 */
@Entity
public class UserDb {

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private String uid;
    private String displayName;
    private Integer timesSent;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public UserDb() {
    }

    public UserDb(Long id) {
        this.id = id;
    }

    @Generated
    public UserDb(Long id, String uid, String displayName, Integer timesSent) {
        this.id = id;
        this.uid = uid;
        this.displayName = displayName;
        this.timesSent = timesSent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getUid() {
        return uid;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setUid(@NotNull String uid) {
        this.uid = uid;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getTimesSent() {
        return timesSent;
    }

    public void setTimesSent(Integer timesSent) {
        this.timesSent = timesSent;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
