package com.antonio.qutestickers.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.antonio.qutestickers.Fragments.ReceivedStickersFragment;
import com.antonio.qutestickers.Helpers.ProgressValueEventListener;
import com.antonio.qutestickers.Models.ReceivedSticker;
import com.antonio.qutestickers.R;
import com.antonio.qutestickers.ViewStickerActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.List;

/**
 * Created by Programmer on 15-May-17.
 */

public class ReceivedStickersAdapter extends RecyclerView.Adapter<ReceivedStickersAdapter.ViewHolder> {

    private List<ReceivedSticker> stickers;
    private Activity act;

    public ReceivedStickersAdapter(Activity act, ReceivedStickersFragment fragment, List<ReceivedSticker> stickers) {
        this.act = act;
        this.stickers = stickers;
    }

    @Override
    public ReceivedStickersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_received_sticker, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ReceivedStickersAdapter.ViewHolder holder, final int position) {
        final String msg = stickers.get(position).getMessage();
        final String author = stickers.get(position).getAuthor();
        final String stickerID = stickers.get(position).getStickerID();

        holder.tvFrom.setText(author);
        holder.tvMessage.setText(msg);

        if (!stickers.get(position).isSeen()) {
            holder.tvFrom.setCompoundDrawablesWithIntrinsicBounds(R.drawable.circle, 0, 0, 0);
        } else {
            holder.tvFrom.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getSticker(stickerID, author, msg);

            }
        });
    }

    private void getSticker(final String stickerID, final String author, final String msg) {

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference mRef = firebaseDatabase.getReference("stickers");
        mRef.keepSynced(true);
        Query query = mRef.orderByChild("stickerURL");
        query.addListenerForSingleValueEvent(new ProgressValueEventListener(act) {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                super.onDataChange(snapshot);
                for (DataSnapshot ds : snapshot.getChildren()) {
                    if (ds.getKey().equalsIgnoreCase(stickerID)) {
                        Intent intent = new Intent(act, ViewStickerActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("author", author);
                        intent.putExtra("stickerURL", ds.child("stickerURL").getValue().toString());
                        intent.putExtra("stickerID", stickerID);
                        intent.putExtra("message", msg);
                        act.startActivity(intent);
                        return;
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                super.onCancelled(error);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stickers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvFrom, tvMessage;
        private LinearLayout linearLayout;

        public ViewHolder(View view) {
            super(view);
            tvFrom = (TextView) view.findViewById(R.id.tv_received_sticker_from);
            tvMessage = (TextView) view.findViewById(R.id.tv_received_sticker_msg);
            linearLayout = (LinearLayout) view.findViewById(R.id.ll_received_sticker);
        }
    }
}
