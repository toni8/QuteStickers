package com.antonio.qutestickers.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.antonio.qutestickers.Database.UserDb;
import com.antonio.qutestickers.Helpers.Constants;
import com.antonio.qutestickers.R;

import java.util.List;

/**
 * Created by Programmer on 15-May-17.
 */

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> {

    private List<UserDb> recentUsers;
    private Context ctx;

    public interface FriendClickedCaller {
        void userChosenFromExistingList(String userName);
    }

    public FriendsAdapter(Context ctx, List<UserDb> recentUsers) {
        this.ctx = ctx;
        this.recentUsers = recentUsers;
    }

    @Override
    public FriendsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friend, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FriendsAdapter.ViewHolder holder, final int position) {
        int timesSent = recentUsers.get(position).getTimesSent() == null ||
                recentUsers.get(position).getTimesSent() == 0 ? 1 :
                recentUsers.get(position).getTimesSent();

        String text = recentUsers.get(position).getDisplayName()
                + " (" + timesSent + ")";

        if (timesSent < Constants.ACHIEVEMENT_YAYZ) {
            holder.ivFriendStatus.setImageResource(R.drawable.jelly_sticker_classic);
        } else if (timesSent < Constants.ACHIEVEMENT_AWESOME) {
            holder.ivFriendStatus.setImageResource(R.drawable.jelly_sticker_yayz);
        } else if (timesSent < Constants.ACHIEVEMENT_ZOMFJ) {
            holder.ivFriendStatus.setImageResource(R.drawable.jelly_sticker_awesome);
        } else {
            holder.ivFriendStatus.setImageResource(R.drawable.jelly_sticker_zomfj);
        }

        holder.tvFriendName.setText(text);

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ctx instanceof FriendClickedCaller) {
                    ((FriendClickedCaller) ctx).userChosenFromExistingList(recentUsers.get(position).getDisplayName());
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return recentUsers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvFriendName;
        private ImageView ivFriendStatus;
        private View parent;

        public ViewHolder(View view) {
            super(view);
            tvFriendName = (TextView) view.findViewById(R.id.tv_friend_name);
            ivFriendStatus = (ImageView) view.findViewById(R.id.iv_friend_status);
            parent = view.findViewById(R.id.ll_item_friend);
        }
    }
}
