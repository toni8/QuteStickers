package com.antonio.qutestickers.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.antonio.qutestickers.Activities.ChooseFriendActivity;
import com.antonio.qutestickers.Helpers.Constants;
import com.antonio.qutestickers.Helpers.Utils;
import com.antonio.qutestickers.Models.Sticker;
import com.antonio.qutestickers.R;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.List;

import pl.droidsonroids.gif.GifImageView;

/**
 * Created by Antonio on 14-May-17.
 */

public class StickerListAdapter extends RecyclerView.Adapter<StickerListAdapter.ViewHolder> {
    private List<Sticker> stickers;
    private Activity activity;
    public StickerListAdapter(Activity activity, List<Sticker> stickers) {
        this.stickers = stickers;
        this.activity = activity;
    }

    @Override
    public StickerListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_sticker, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final StickerListAdapter.ViewHolder viewHolder, int i) {

        if(stickers.get(i).getStickerURL().contains("gif") || stickers.get(i).getStickerURL().contains("png")) {
            Ion.with(viewHolder.gifImageView).load(stickers.get(i).getStickerURL()).setCallback(new FutureCallback<ImageView>() {
                @Override
                public void onCompleted(Exception e, ImageView result) {
                    viewHolder.progressBar.setVisibility(View.GONE);
                }
            });
        } else {
            viewHolder.gifImageView.setBackgroundResource(Utils.getDrawableId(activity, stickers.get(i).getStickerURL()));
            viewHolder.progressBar.setVisibility(View.GONE);
        }
        viewHolder.gifImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                activity.startActivity(new Intent(activity, ChooseFriendActivity.class).putExtra(Constants.EXTRA_STICKER_URL,
                        stickers.get(viewHolder.getAdapterPosition()).getStickerURL()));
            }
        });
    }


    @Override
    public int getItemCount() {
        return stickers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private GifImageView gifImageView;
        private ProgressBar progressBar;

        public ViewHolder(View view) {
            super(view);

            gifImageView = (GifImageView) view.findViewById(R.id.gif_iv_sticker);
            progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        }
    }

}