package com.antonio.qutestickers.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.antonio.qutestickers.Fragments.LocalStickersFragment;
import com.antonio.qutestickers.Fragments.OnlineStickersFragment;
import com.antonio.qutestickers.Fragments.ReceivedStickersFragment;

/**
 * Created by Programmer on 11-May-17.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                LocalStickersFragment tab1 = new LocalStickersFragment();
                return tab1;
            case 1:
                OnlineStickersFragment tab2 = new OnlineStickersFragment();
                return tab2;
            case 2:
                ReceivedStickersFragment tab3 = new ReceivedStickersFragment();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}